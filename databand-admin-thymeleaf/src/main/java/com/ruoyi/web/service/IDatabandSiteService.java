package com.ruoyi.web.service;

import java.util.List;
import com.ruoyi.web.domain.DatabandSite;

/**
 * 站点Service接口
 * 
 * @author databand
 * @date 2020-12-31
 */
public interface IDatabandSiteService 
{
    /**
     * 查询站点
     * 
     * @param id 站点ID
     * @return 站点
     */
    public DatabandSite selectDatabandSiteById(Long id);

    /**
     * 查询站点列表
     * 
     * @param databandSite 站点
     * @return 站点集合
     */
    public List<DatabandSite> selectDatabandSiteList(DatabandSite databandSite);

    /**
     * 新增站点
     * 
     * @param databandSite 站点
     * @return 结果
     */
    public int insertDatabandSite(DatabandSite databandSite);

    /**
     * 修改站点
     * 
     * @param databandSite 站点
     * @return 结果
     */
    public int updateDatabandSite(DatabandSite databandSite);

    /**
     * 批量删除站点
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDatabandSiteByIds(String ids);

    /**
     * 删除站点信息
     * 
     * @param id 站点ID
     * @return 结果
     */
    public int deleteDatabandSiteById(Long id);
}
