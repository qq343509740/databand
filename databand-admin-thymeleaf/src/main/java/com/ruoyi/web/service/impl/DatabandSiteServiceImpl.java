package com.ruoyi.web.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.web.mapper.DatabandSiteMapper;
import com.ruoyi.web.domain.DatabandSite;
import com.ruoyi.web.service.IDatabandSiteService;
import com.ruoyi.common.core.text.Convert;

/**
 * 站点Service业务层处理
 * 
 * @author databand
 * @date 2020-12-31
 */
@Service
public class DatabandSiteServiceImpl implements IDatabandSiteService 
{
    @Autowired
    private DatabandSiteMapper databandSiteMapper;

    /**
     * 查询站点
     * 
     * @param id 站点ID
     * @return 站点
     */
    @Override
    public DatabandSite selectDatabandSiteById(Long id)
    {
        return databandSiteMapper.selectDatabandSiteById(id);
    }

    /**
     * 查询站点列表
     * 
     * @param databandSite 站点
     * @return 站点
     */
    @Override
    public List<DatabandSite> selectDatabandSiteList(DatabandSite databandSite)
    {
        return databandSiteMapper.selectDatabandSiteList(databandSite);
    }

    /**
     * 新增站点
     * 
     * @param databandSite 站点
     * @return 结果
     */
    @Override
    public int insertDatabandSite(DatabandSite databandSite)
    {
        return databandSiteMapper.insertDatabandSite(databandSite);
    }

    /**
     * 修改站点
     * 
     * @param databandSite 站点
     * @return 结果
     */
    @Override
    public int updateDatabandSite(DatabandSite databandSite)
    {
        return databandSiteMapper.updateDatabandSite(databandSite);
    }

    /**
     * 删除站点对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteDatabandSiteByIds(String ids)
    {
        return databandSiteMapper.deleteDatabandSiteByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除站点信息
     * 
     * @param id 站点ID
     * @return 结果
     */
    @Override
    public int deleteDatabandSiteById(Long id)
    {
        return databandSiteMapper.deleteDatabandSiteById(id);
    }
}
