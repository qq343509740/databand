package com.ruoyi.web.service;

import java.util.List;
import com.ruoyi.web.domain.DatabandSource;

/**
 * 数据源Service接口
 * 
 * @author databand
 * @date 2020-12-31
 */
public interface IDatabandSourceService 
{
    /**
     * 查询数据源
     * 
     * @param id 数据源ID
     * @return 数据源
     */
    public DatabandSource selectDatabandSourceById(Long id);

    /**
     * 查询数据源列表
     * 
     * @param databandSource 数据源
     * @return 数据源集合
     */
    public List<DatabandSource> selectDatabandSourceList(DatabandSource databandSource);
    public List<DatabandSource> selectDatabandSourceList();

    /**
     * 新增数据源
     * 
     * @param databandSource 数据源
     * @return 结果
     */
    public int insertDatabandSource(DatabandSource databandSource);

    /**
     * 修改数据源
     * 
     * @param databandSource 数据源
     * @return 结果
     */
    public int updateDatabandSource(DatabandSource databandSource);

    /**
     * 批量删除数据源
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDatabandSourceByIds(String ids);

    /**
     * 删除数据源信息
     * 
     * @param id 数据源ID
     * @return 结果
     */
    public int deleteDatabandSourceById(Long id);
}
