package com.ruoyi.web.mapper;

import java.util.List;
import com.ruoyi.web.domain.DatabandScheduletask;

/**
 * 批处理计划Mapper接口
 * 
 * @author databand
 * @date 2020-12-31
 */
public interface DatabandScheduletaskMapper 
{
    /**
     * 查询批处理计划
     * 
     * @param id 批处理计划ID
     * @return 批处理计划
     */
    public DatabandScheduletask selectDatabandScheduletaskById(Long id);

    /**
     * 查询批处理计划列表
     * 
     * @param databandScheduletask 批处理计划
     * @return 批处理计划集合
     */
    public List<DatabandScheduletask> selectDatabandScheduletaskList(DatabandScheduletask databandScheduletask);

    /**
     * 新增批处理计划
     * 
     * @param databandScheduletask 批处理计划
     * @return 结果
     */
    public int insertDatabandScheduletask(DatabandScheduletask databandScheduletask);

    /**
     * 修改批处理计划
     * 
     * @param databandScheduletask 批处理计划
     * @return 结果
     */
    public int updateDatabandScheduletask(DatabandScheduletask databandScheduletask);

    /**
     * 删除批处理计划
     * 
     * @param id 批处理计划ID
     * @return 结果
     */
    public int deleteDatabandScheduletaskById(Long id);

    /**
     * 批量删除批处理计划
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDatabandScheduletaskByIds(String[] ids);
}
