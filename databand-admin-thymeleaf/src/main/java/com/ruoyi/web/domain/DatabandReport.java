package com.ruoyi.web.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 报对象 databand_report
 * 
 * @author databand
 * @date 2020-12-31
 */
public class DatabandReport extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;
    private Long reportid;



	/** 报表标题 */
    @Excel(name = "报表标题")
    private String title;

    /** 描述 */
    @Excel(name = "描述")
    private String descri;

    /** $column.columnComment */
    @Excel(name = "描述")
    private Long templateid;

    /** $column.columnComment */
    @Excel(name = "描述")
    private String reporturl;

    /** $column.columnComment */
    @Excel(name = "描述")
    private String icon;

    /** 二级标题 */
    @Excel(name = "二级标题")
    private String subtitle;

    /** 时间日期范围 */
    @Excel(name = "时间日期范围")
    private String daterange;

    /** 下拉选项(字典项) */
    @Excel(name = "下拉选项(字典项)")
    private String options;

    /** 报表SQL */
    @Excel(name = "报表SQL")
    private String sql;

    /** 列表展示SQL */
    @Excel(name = "列表展示SQL")
    private String listsql;

    /** 数据源ID */
    @Excel(name = "数据源ID")
    private Long sourceid;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    
    public Long getReportid() {
		return id;
	}

	public void setReportid(Long reportid) {
		this.reportid = this.id;
	}
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setDescri(String descri) 
    {
        this.descri = descri;
    }

    public String getDescri() 
    {
        return descri;
    }
    public void setTemplateid(Long templateid) 
    {
        this.templateid = templateid;
    }

    public Long getTemplateid() 
    {
        return templateid;
    }
    public void setReporturl(String reporturl) 
    {
        this.reporturl = reporturl;
    }

    public String getReporturl() 
    {
        return reporturl;
    }
    public void setIcon(String icon) 
    {
        this.icon = icon;
    }

    public String getIcon() 
    {
        return icon;
    }
    public void setSubtitle(String subtitle) 
    {
        this.subtitle = subtitle;
    }

    public String getSubtitle() 
    {
        return subtitle;
    }
    public void setDaterange(String daterange) 
    {
        this.daterange = daterange;
    }

    public String getDaterange() 
    {
        return daterange;
    }
    public void setOptions(String options) 
    {
        this.options = options;
    }

    public String getOptions() 
    {
        return options;
    }
    public void setSql(String sql) 
    {
        this.sql = sql;
    }

    public String getSql() 
    {
        return sql;
    }
    public void setListsql(String listsql) 
    {
        this.listsql = listsql;
    }

    public String getListsql() 
    {
        return listsql;
    }
    public void setSourceid(Long sourceid) 
    {
        this.sourceid = sourceid;
    }

    public Long getSourceid() 
    {
        return sourceid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("descri", getDescri())
            .append("templateid", getTemplateid())
            .append("reporturl", getReporturl())
            .append("icon", getIcon())
            .append("subtitle", getSubtitle())
            .append("daterange", getDaterange())
            .append("options", getOptions())
            .append("sql", getSql())
            .append("listsql", getListsql())
            .append("sourceid", getSourceid())
            .toString();
    }
}
