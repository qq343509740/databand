package org.databandtech.mysql2clickhouse.source;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.source.RichSourceFunction;

public class SourceFromMySQLByTuple5 extends RichSourceFunction<Tuple5<String,Integer,String,String,String>> {

	private static final long serialVersionUID = 3120905365885782365L;
	PreparedStatement preparedStatement;
    Connection connection;
    String[] COLUMNS;
    String SQL;
    String URL = "";
    String USERNAME = "";
    String PASSWORD = "";
    
    public SourceFromMySQLByTuple5(String url, String username,String password,String[] columns,String sql) {
        super();
        this.COLUMNS = columns;
        this.SQL = sql;
        this.URL = url;
        this.USERNAME = username;
        this.PASSWORD = password;
    }

    @Override
    public void open(Configuration parameters) {
        String driver = "com.mysql.jdbc.Driver";        
        try {
			super.open( parameters );
	        Class.forName( driver );
	        connection = DriverManager.getConnection( URL, USERNAME, PASSWORD );
	        String sql = SQL;
	        preparedStatement = connection.prepareStatement( sql );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Override
    public void close()  {
        try {
			super.close();
			if (connection != null) {
	            connection.close();
	        }
	        if (preparedStatement != null) {
	            preparedStatement.close();
	        }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}       
    }

    public void run(SourceContext<Tuple5<String,Integer,String,String,String>> sourceContext)  {
        try {
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while (resultSet.next()) {
            	Tuple5<String,Integer,String,String,String> obj = new Tuple5<String,Integer,String,String,String>();
            	obj.setField(resultSet.getString(COLUMNS[0]).trim(), 0);
            	obj.setField(resultSet.getInt(COLUMNS[1]), 1);
            	obj.setField(resultSet.getString(COLUMNS[2]), 2);
            	obj.setField(resultSet.getString(COLUMNS[3]), 3);
            	obj.setField(resultSet.getString(COLUMNS[4]), 4);
            	sourceContext.collect( obj );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cancel() {

    }

}
