/*
Navicat MySQL Data Transfer

Source Server         : my57
Source Server Version : 50722
Source Host           : localhost:3306
Source Database       : databand

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2020-11-03 22:21:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `databand_mockinstances`
-- ----------------------------
DROP TABLE IF EXISTS `databand_mockinstances`;
CREATE TABLE `databand_mockinstances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descrb` varchar(300) DEFAULT NULL,
  `method` varchar(10) DEFAULT NULL,
  `path` varchar(200) DEFAULT NULL,
  `path_parameters` varchar(300) DEFAULT NULL,
  `query_string_parameters` varchar(3000) DEFAULT NULL,
  `req_cookie` varchar(300) DEFAULT NULL,
  `req_headers` varchar(500) DEFAULT NULL,
  `req_jsonbody` text,
  `resp_statuscode` varchar(50) DEFAULT NULL,
  `resp_cookie` varchar(300) DEFAULT NULL,
  `resp_headers` varchar(500) DEFAULT NULL,
  `resp_body` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of databand_mockinstances
-- ----------------------------
INSERT INTO `databand_mockinstances` VALUES ('1', 'GET路径：/mypath/2/ANYCHARS', 'GET', '/mypath/{id}/{type}', '{id<=>2}$$${type<=>[A-Z0-9\\\\\\\\-]+}', null, null, null, '', '200', '{Session:97d43b1e-fe03-4855-926a-f448eddac32f}', '{Content-Type:text/html;charset=utf-8},{other:othervalue}', '{\r\n  \"id\" : 1,\r\n  \"name\" : \"姓名\",\r\n  \"price\" : \"123\",\r\n  \"price2\" : \"121\",\r\n  \"enabled\" : \"true\",\r\n  \"tags\" : [ \"tag1\", \"tag2数组项\" ]\r\n}');
INSERT INTO `databand_mockinstances` VALUES ('2', 'GET路径：/mypath2?month=10&userid=ANYCHARS', 'GET', '/mypath2', null, '{month<=>10}$$${userid<=>[A-Z0-9\\\\\\\\-]+}', null, null, '', '200', null, '{Content-Type:text/html;charset=utf-8},{other:othervalue}', '{\r\n  \"id\" : \"97d43b1e-fe03-4855-926a-f448eddac32f\",\r\n  \"name\" : \"姓名\",\r\n  \"year\" : \"2019\",\r\n  \"month\" : \"10\",\r\n  \"userid\" : \"id\",\r\n  \"tags\" : [ \"tag3\", \"tag4\" ]\r\n}');
INSERT INTO `databand_mockinstances` VALUES ('3', 'GET路径：/get', 'GET', '/get', null, null, null, null, '{}', '200', null, null, '{\r\n  \"id\" : 1,\r\n  \"name\" : \"姓名\",\r\n  \"price\" : \"123\",\r\n  \"price2\" : \"121\",\r\n  \"enabled\" : \"true\",\r\n  \"tags\" : [ \"home\", \"green\" ]\r\n}');
INSERT INTO `databand_mockinstances` VALUES ('4', 'POST路径：/submitForm', 'POST', '/submitForm', null, null, null, null, '{\"username\": \"user\",  \"password\": \"mypass123\"}', '200', null, null, '{\r\n  \"id\" : 123,\r\n  \"name\" : \"post ok\"\r\n}');
