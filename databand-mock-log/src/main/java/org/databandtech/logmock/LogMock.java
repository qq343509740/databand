package org.databandtech.logmock;

import org.databandtech.common.Mock;
import org.databandtech.logmock.logback.LogService;


/**
 * log生成简单demo，生成路径见配置文件：logback.xml。 win下默认“c:/logs/”，linux 或 mac下路径请自行修改
 * @author wangixn
 *
 */
public class LogMock {

	static int COUNT = 10000;
	
	public static void main( String[] args )
    {	
		for(int i=0;i<COUNT;i++) {
			LogService.info(Mock.getChineseName()+","+
					Mock.getDate()+","+
					Mock.getEmail(4, 6)+","+
					Mock.getRoad()+","+
					Mock.getNumString(90)+","+
					Mock.getNumString(50)+","+
					Mock.getNumString(300));
		}
		
    }
	
}
